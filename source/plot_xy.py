"""
Plotting script for programming exercises of lecture 'Programmierpraktikum zur
Numerischen Wettervorhersage'.

Tobias Gronemeier - 2019-02-04
"""

import numpy as np

import matplotlib
import matplotlib.pyplot as plt

from netCDF4 import Dataset

import os

#------------------------------------------------------------------------------#

print("start plotting")

#define plot
inputfile = "../output/nwp_output.nc"
outputfile = "../images/forecast/nwp_48.png"

#read data from output file
ncfile = Dataset(inputfile, mode='r')

x = ncfile.variables['x'][:] * 0.001
y = ncfile.variables['y'][:] * 0.001
time = ncfile.variables['time'][:] / 3600.0

phi = ncfile.variables['phi'][:,:,:]
phi_ref = ncfile.variables['phi_ref'][:,:,:]
zeta = ncfile.variables['zeta'][:,:,:]
u = ncfile.variables['u'][:,:,:]
v = ncfile.variables['v'][:,:,:]

#create plot
plt.suptitle('Horizontal section after 48 hours', fontsize = 20)
cmap = plt.get_cmap('bwr')
plt.grid(True)

plot1 = plt.subplot(2, 2, 1)
plt.contourf(x, y, phi[0,:,:], cmap=cmap)
plot1.set(title = 'Geopotential', xlabel = 'x (km)', ylabel = 'y (km)')

plot2 = plt.subplot(2, 2, 1)
plt.contour(x, y, phi_ref[0,:,:])

plot3 = plt.subplot(2, 2, 2)
plt.contourf(x, y, zeta[0,:,:], cmap=cmap)
plot3.set(title = 'Vorticity', xlabel = 'x (km)', ylabel = 'y (km)')

plot4 = plt.subplot(2, 2, 3)
plt.contourf(x, y, u[0,:,:], cmap=cmap)
plot4.set(title = 'Wind in x direction', xlabel = 'x (km)', ylabel = 'y (km)')

plot5 = plt.subplot(2, 2, 4)
plt.contourf(x, y, v[0,:,:], cmap=cmap)
plot5.set(title = 'Wind in y direction', xlabel = 'x (km)', ylabel = 'y (km)')

plt.subplots_adjust(left=0.15, bottom=0.1, right=0.9, top=0.85, wspace=0.5,
		    hspace=0.5)

plt.savefig(outputfile)
plt.show()

print("finished plotting")
