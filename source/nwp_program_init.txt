&INIT
  exercise = 1,
  file_in = "../input/era_int_eu_20171024_20171026.nc",
  file_out = "../output/nwp_output",
  delta = 60000,
  k_max = 10000,
  latitude = 52.5
  nx = 90,
  ny = 58,
  sheet = 3,
  time_dt_do = 0.5,
  time_end = 48.0
/
