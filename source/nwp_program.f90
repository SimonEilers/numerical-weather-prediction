!-------------------------------------------------------------------------------
! NUMERICAL WEATHER PREDICTION
!-------------------------------------------------------------------------------
! DESCRIPTION:
!   The purpose of this program is to numerically predict the weather in the
!   500 hPa Niveau from a given data set with a barotropic model. The specific
!   parameters for a forecast can be set in the file "nwp_program_init.txt". The
!   program produces an output file called "nwp_output.nc" that shows the time
!   evolution of the geopotential, the vorticity and the wind field.
!-------------------------------------------------------------------------------
! Author: Simon Eilers (template by Tobias Gronemeier)
! Date  : 2019-03-04
!-------------------------------------------------------------------------------
! NOTE: Code compilation
!   When compiling the code, NetCDF libraries must be included/linked by
!   "ifort nwp_program.f90 -o nwp_program -I <path-to-netcdf>/include
!   -L <path-to-netcdf>/lib -lnetcdff".
!-------------------------------------------------------------------------------

PROGRAM nwp_program

  USE netcdf

  IMPLICIT NONE

  !--------------------------------Initialization-------------------------------
  
  CHARACTER(LEN=100) :: file_in     !< name of input file
  CHARACTER(LEN=100) :: file_out    !< name of output file

  INTEGER :: count_total            !< count (needed for total runtime)
  INTEGER :: count_rate_total       !< count rate (needed for total runtime)
  INTEGER :: exercise = 1           !< exercise number (namelist param)
  INTEGER :: i                      !< loop index
  INTEGER :: ioerr                  !< error param
  INTEGER :: j                      !< loop index
  INTEGER :: k                      !< loop index
  INTEGER :: k_max = 10000          !< maximum iteration count of SOR method
                                    !< (namelist param)
  INTEGER :: min_begin(3)           !< position of minimum geopotential at begin
  INTEGER :: min_end(2)             !< position of minimum geopotential at end
  INTEGER :: nt_ref = 8             !< number of time steps for reference
                                    !< geopotential
  INTEGER :: nx = 90                !< number of grid points along x
                                    !< (namelist param)
  INTEGER :: ny = 58                !< number of grid points along y
                                    !< (namelist param)
  INTEGER :: sheet = 3              !< sheet number (namelist param)
  INTEGER :: t = 0                  !< step count for time iteration
  
  REAL :: beta                      !< beta param
  REAL :: delta = 60000.0           !< grid width (namelist param)
  REAL :: dt                        !< timestep
  REAL :: dt_do                     !< timestep of data output
  REAL :: f_0                       !< Coriolis param at y=0
  REAL :: latitude = 30.0           !< latitude at y=0 (namelist param)
  REAL :: lx                        !< wavelenght in x direction
  REAL :: ly                        !< wavelenght in y direction
  REAL :: runtime_bound = 0.0       !< runtime for calc of boundary values
  REAL :: runtime_geo = 0.0         !< runtime for calc of geopotential
  REAL :: runtime_output = 0.0      !< runtime for data output
  REAL :: runtime_total = 0.0       !< total runtime
  REAL :: runtime_vor = 0.0         !< runtime for calc of vorticity
  REAL :: runtime_wind = 0.0        !< runtime for calc of wind
  REAL :: t_end                     !< time to simulate (s)
  REAL :: t_sim = 0.0               !< simulated time (s)
  REAL :: time_dt_do = 0.5          !< time interval of output (h, namelist
                                    !< param)
  REAL :: time_end = 48.0           !< time to simulate (h, namelist param)
  REAL :: u_bg                      !< background wind speed
  REAL :: u_max                     !< maximum wind speed

  REAL, PARAMETER :: omega_0 = 7.29212E-5    !< angular speed of earth
  REAL, PARAMETER :: pi = 3.14159            !< pi
  REAL, PARAMETER :: radius = 6.371E6        !< mean radius of earth

  REAL, ALLOCATABLE :: f(:)                  !< Coriolis parameter along y
  REAL, ALLOCATABLE :: u(:,:)                !< wind speed along x
  REAL, ALLOCATABLE :: v(:,:)                !< wind speed along y
  REAL, ALLOCATABLE :: zeta_m(:,:)           !< vorticity at time t-1
  REAL, ALLOCATABLE :: zeta_p(:,:)           !< vorticity at time t+1
  
  REAL(kind=8), ALLOCATABLE :: phi(:,:)      !< geopotential
  REAL(kind=8), ALLOCATABLE :: zeta(:,:)     !< vorticity at time t

  REAL, ALLOCATABLE :: phi_ref(:,:,:)        !< reference geopotential

  !- Define initialization namelist
  NAMELIST / INIT / exercise, file_in, file_out, delta, k_max, latitude, nx, &
    ny, sheet, time_end, time_dt_do

  !- Gauge total runtime
  CALL SYSTEM_CLOCK(count_total, count_rate_total)
  runtime_total = REAL(count_total) / REAL(count_rate_total)

  WRITE(*, '(A//A)') '--- NWP program ---', '--- Start forecast ---'

  !- Open initialization file
  OPEN(20, FILE='nwp_program_init.txt', STATUS='OLD', FORM='FORMATTED', &
    IOSTAT=ioerr)
  IF (ioerr > 0) THEN
    CALL error_message('init_open')
  END IF

  !- Read initialization file
  READ(20, NML=INIT, IOSTAT=ioerr)
  IF (ioerr > 0) THEN
    CALL error_message('init_read')
  END IF

  CLOSE(20)

  !- Call error message if namelist parameter have unreasonable values
  IF (delta <= 0 .OR. k_max <= 0 .OR. latitude < -90.0 .OR. latitude > 90.0 &
    .OR. nx <= 0 .OR. ny <= 0 .OR. time_end < 0 .OR. time_dt_do <= 0) THEN
    CALL error_message('init_data')
  END IF
    
  !- Allocate arrays
  ALLOCATE(f(0:ny))
  ALLOCATE(phi(-1:nx+1,-1:ny+1))
  ALLOCATE(phi_ref(-1:nx+1,-1:ny+1,0:nt_ref))
  ALLOCATE(u(0:nx,0:ny))
  ALLOCATE(v(0:nx,0:ny))
  ALLOCATE(zeta(-1:nx+1,0:ny))
  ALLOCATE(zeta_m(-1:nx+1,0:ny))
  ALLOCATE(zeta_p(-1:nx+1,0:ny))
  
  !- Initialize arrays depending on exercise sheet
  SELECT CASE (sheet)

  CASE (1)
  ! Sheet 1 (test of SOR method)
    
    f(:) = 0.0001
    phi(:,:) = 0.0

    !- Initialize arrays depending on exercise
    SELECT CASE (exercise)

    CASE (1)
    ! Exercise 1 (constant velocity field)
 
      phi(:,0) = 0.0
      phi(:,ny) = 50.0
      zeta(:,:) = 0.0

      ! Initialize reference geopotential
      DO j=0,ny
        phi_ref(:,j,0) = 50.0 * j / ny
      END DO
      
    CASE (2)
    ! Exercise 2 (shear flow)

      zeta(:,:) = 0.0001

      !- Initialize reference geopotential
      DO j=0,ny
        phi_ref(:,j,0) = 0.5 * f(j) * zeta(:,j) * ((j * delta)**2 - j * ny &
          * delta**2)
      END DO

    CASE (3)
    ! Exercise 3 (Haurwitz wave)

      lx = nx * delta
      ly = 2 * ny * delta
      u_max = 10.0

      !- Initialize reference geopotential and vorticity
      DO j=0,ny
        DO i=0,nx
          phi_ref(i,j,0) = u_max * (f(j) * ly / (2 * pi)) * SIN(2 * pi * i &
            * delta / lx) * SIN(2 * pi * j * delta / ly)
          zeta(i,j) = -((4 * pi**2 / lx**2) + (4 * pi**2 / ly**2)) / f(j) &
            * phi_ref(i,j,0)
        END DO
      END DO

      !- Set cyclic boundary conditions for vorticity
      zeta(-1,:) = zeta(nx,:)
      zeta(nx+1,:) = zeta(0,:)

    CASE DEFAULT
    ! Unknown exercise

      WRITE (*, '(A)') 'Unknown exercise on sheet 1.'
      STOP

    END SELECT

    !- Calculate geopotential and wind 
    CALL get_geopot()
    CALL get_wind()

  CASE (2)
  ! Sheet 2 (test of timestep method)
    
    beta = 2 * omega_0 * COS(2 * pi * latitude / 360) / radius
    dt_do = time_dt_do * 3600
    f_0 = 2 * omega_0 * SIN(2 * pi * latitude / 360)
    lx = nx * delta
    ly = 2 * ny * delta
    phi(:,:) = 0.0
    t_end = time_end * 3600
    u_max = 10.0

    !- Initialize arrays depending on exercise
    SELECT CASE (exercise)

    CASE (1)
    ! Exercise 1 (evolution of Haurwitz wave with constant Coriolis parameter) 

      f(:) = f_0

    CASE (2)
    ! Exercise 2 (evolution of Haurwitz wave with variable Coriolis parameter)

      !- Set Coriolis parameter
      DO j=0,ny
        f(j) = f_0 + beta * (j - ny / 2) * delta
      END DO

    CASE (3)
    ! Exercise 3 (variation of background speed)

      !- Set Coriolis parameter
      DO j=0,ny
        f(j) = f_0 + beta * (j - ny / 2) * delta
      END DO

      !- Set background wind and boundary conditions for geopotential
      u_bg = 25.11
      phi(:,0) = ny * delta * f_0 * u_bg / 2
      phi(:,ny) = -ny * delta * f_0 * u_bg / 2
    
    CASE (4)
    ! Exercise 4 (variation of wavelenght)
      
      lx = 0.5 * nx * delta

      !- Set Coriolis parameter
      DO j=0,ny
        f(j) = f_0 + beta * (j - ny / 2) * delta
      END DO
      
    CASE DEFAULT
    ! Unknown exercise

      WRITE (*, '(A)') 'Unknown exercise on sheet 2.'
      STOP

    END SELECT
    
    !- Initialize vorticity 
    DO j=0,ny
      DO i=0,nx
        zeta(i,j) = -((4 * pi**2 / lx**2) + (4 * pi**2 / ly**2)) / f(j) &
          * u_max * f(j) * ly / (2 * pi) * SIN(2 * pi * i * delta / lx) &
          * SIN(2 * pi * j * delta / ly)
      END DO
    END DO
    
    !- Set cyclic boundary conditions for vorticity
    zeta(-1,:) = zeta(nx,:)
    zeta(nx+1,:) = zeta(0,:)

    !- Calculate geopotential and set reference geopotential
    CALL get_geopot()
    phi_ref(:,:,0) = phi(:,:)
    
    !- Calculate wind
    CALL get_wind()

  CASE (3)
  ! Sheet 3 (weather forecast)
    
    beta = 2 * omega_0 * COS(2 * pi * latitude / 360) / radius
    dt_do = time_dt_do * 3600
    f_0 = 2 * omega_0 * SIN(2 * pi * latitude / 360)
    t_end = time_end * 3600
    
    !- Call error message if end time is bigger than 48h
    IF (time_end > 48.0) THEN
      CALL error_message('time_end')
    END IF
    
    !- Read input file
    CALL read_input_data(file_in)
    
    phi(:,:) = phi_ref(:,:,0)
    
    !- Calculate Coriolis parameter and vorticity
    DO j=0,ny
      f(j) = f_0 + beta * (j - ny / 2) * delta
      DO i=0,nx
        zeta(i,j) = 1 / f(j) * ((phi(i+1,j) - 2 * phi(i,j) + phi(i-1,j)) &
          / delta**2 + (phi(i,j+1) - 2 * phi(i,j) + phi(i,j-1)) / delta**2)
      END DO
    END DO

    !- Calculate wind
    CALL get_wind()
  
  CASE DEFAULT
  ! Unknown sheet

      WRITE (*, '(A)') 'Unknown sheet.'
      STOP

  END SELECT

  !----------------------------------Forecast-----------------------------------
  
  !- Open and write output file at t=0
  CALL data_output('open', file_out)
  CALL data_output('write', file_out)

  !- Do simulation
  DO WHILE (t_sim < t_end .AND. sheet /= 1)
    
    !- Calculate timestep
    dt = 0.5 * MIN(delta / MAXVAL(u(:,:)), delta / MAXVAL(v(:,:)))
    
    !- Set new step count and simulation time  
    t = t + 1
    t_sim = t_sim + dt
    
    !- Write simulation time (h)
    WRITE (*, '(A,F6.2)') 'sim. time: ', t_sim / 3600

    !- Calculate vorticity
    CALL get_vorticity()
    
    !- Calculate boundary values
    IF (sheet == 3) THEN
      CALL get_boundary()
    END IF
    
    !- Calculate geopotential and wind
    CALL get_geopot()
    CALL get_wind()
    
    !- Write output file if simulated time is bigger or equal than a multiple of
    !- timestep of data output 
    IF (INT(t_sim / dt_do) /= INT((t_sim - dt) / dt_do)) THEN
      CALL data_output('write', file_out)
    END IF

  END DO

  !- Finalize output
  CALL data_output('close', file_out)
  
  IF (sheet == 2 .AND. exercise == 2) THEN

    !- Calculate position of minimum geopotential at begin and end
    min_begin(:) = MINLOC(phi_ref(:,:,:))
    min_end(:) = MINLOC(phi(:,:))

    !- Write measured and theoretical displacement speed (m/s)
    WRITE(*, '(A, F8.4)') 'displacement speed (measured):    ', &
      delta * (min_end(1) - min_begin(1)) / (t_sim - dt)
    WRITE(*, '(A, F8.4)') 'displacement speed (theoretical): ', &
      -beta / ((2 * pi / lx)**2 + (2 * pi/ ly)**2)

  END IF

  !- Calculate total runtime
  CALL SYSTEM_CLOCK(count_total, count_rate_total)
  runtime_total = REAL(count_total) / REAL(count_rate_total) - runtime_total

  !- Write runtime for eauch process and total runtime (s, %)
  WRITE(*, '(A, F8.4, A, F8.4)') 'runtime SOR method (s, %):       ', &
    runtime_geo, ', ', runtime_geo / runtime_total
  WRITE(*, '(A, F8.4, A, F8.4)') 'runtime timestep method (s, %):  ', &
    runtime_vor, ', ', runtime_vor / runtime_total
  WRITE(*, '(A, F8.4, A, F8.4)') 'runtime wind calculation (s, %): ', &
    runtime_wind, ', ',  runtime_wind / runtime_total
  WRITE(*, '(A, F8.4, A, F8.4)') 'runtime boundary values (s, %):  ', &
    runtime_bound, ', ', runtime_bound / runtime_total
  WRITE(*, '(A, F8.4, A, F8.4)') 'runtime data output (s, %):      ', &
    runtime_output, ', ', runtime_output / runtime_total
  WRITE(*, '(A, F8.4, A, F8.4)') 'runtime total (s, %):            ', &
    runtime_total, ', ', 1.0

  !- Deallocate arrays
  DEALLOCATE(f)
  DEALLOCATE(phi)
  DEALLOCATE(phi_ref)
  DEALLOCATE(u)
  DEALLOCATE(v)
  DEALLOCATE(zeta)
  DEALLOCATE(zeta_m)
  DEALLOCATE(zeta_p)

  WRITE(*, '(A)') '--- Finished! ---'

CONTAINS

!------------------------------------Spline-------------------------------------

  REAL FUNCTION spline(n, y, h, x)
  !- Calculate spline function from reference geopotential and return value at
  !- requested time point
  
    INTEGER :: n    !< number of given time points
    INTEGER :: s    !< number of subfunction
    
    REAL :: h    !< distance of given time points
    REAL :: x    !< requested time point
  
    REAL :: a(0:n)      !< constant term of equation for Thomas algorithm
    REAL :: c(0:n-1)    !< linear coefficients of subfunctions
    REAL :: d(0:n-1)    !< constant coefficients of subfunctions
    REAL :: m(0:n)      !< cubic coefficients of subfunctions
    REAL :: p(0:n-1)    !< modified coefficients of Thomas algorithm
    REAL :: q(0:n)      !< modified coefficients of Thomas algorithm
    REAL :: y(0:n)      !< values of reference geopotential at given time points
    
    !- Calculate cubic coefficients of subfunctions with Thomas algorithm for
    !- natural boundary conditions
    
    !- Calculate constant term of equation for Thomas algorithm
    a(0) = 0.0
    DO k=1,n-1
      a(k) = 6 * (y(k+1) - 2 * y(k) + y(k-1)) / h**2
    END DO
    a(n) = 0.0
    
    !- Calculate modified coefficients of Thomas algorithm
    p(0) = 0.0
    DO k=1,n-1
      p(k) = 1 / (4 - p(k-1))
    END DO
    
    !- Calculate modified coefficients of Thomas algorithm
    q(0) = a(0)
    DO k=1,n-1
      q(k) = (a(k) - q(k-1)) / (4 - p(k-1))
    END DO
    q(n) = a(n)
    
    !- Calculate cubic coefficients
    m(n) = q(n)
    DO k=n,0,-1
      m(k) = q(k) - p(k) * m(k+1)
    END DO
    
    !- Calculate linear and constant coefficients of subfunctions
    DO k=0,n-1
      c(k) = (y(k+1) - y(k)) / h - h * (m(k+1) - m(k)) / 6
      d(k) = y(k) - h**2 * m(k) / 6
    END DO
    
    !- Calculate number of subfunction
    s = INT(x / h)
    
    !- Calculate value of spline function at requested time point
    IF (s == n) THEN
      spline = y(n)
    ELSE
      spline = (((s+1) * h - x)**3 * m(s) / h + (x - s * h)**3 * m(s+1) / h) &
        / 6 + c(s) * (x - s * h) + d(s)
    END IF
    
  END FUNCTION spline

  !------------------------------------Error------------------------------------

  SUBROUTINE error_message(error)
  ! Print error message and close program

    CHARACTER(LEN=*) :: error    !< name of error

    IF (error == 'init_open') THEN
      WRITE(*, '(A)') 'Program failed to open initialization file.'
    END IF

    IF (error == 'init_read') THEN
      WRITE(*, '(A)') 'Program failed to read initialization file.'
    END IF
    
    IF (error == 'init_data') THEN
      WRITE(*, '(A)') 'Namelist parameter have unreasonable values. Please &
        &check initialization file.'
    END IF
    
    IF (error == 'time_end') THEN
      WRITE(*, '(A)') 'End time is too big. Please enter an end time lower or &
        &equal than 48h in initialization file.'
    END IF
    
    STOP

  END SUBROUTINE error_message
  
  !----------------------------------Boundary-----------------------------------
  
  SUBROUTINE get_boundary()
  ! Calculate boundary values for geopotential and vorticity
    
    INTEGER :: count_bound         !< count (needed for runtime)
    INTEGER :: count_rate_bound    !< count rate (needed for runtime)

    REAL :: diff = 0.0             !< difference (needed for runtime)

    !- Gauge runtime
    CALL SYSTEM_CLOCK(count_bound, count_rate_bound)
    diff = REAL(count_bound) / REAL(count_rate_bound)
    
    !- Calculate boundary values of geopotential from reference geopotential
    !- with spline interpolation
    DO j=-1,ny+1
      DO i=-1,1
        phi(i,j) = spline(8, phi_ref(i,j,:), 21600.0, t_sim)
      END DO
    END DO
    DO j=-1,ny+1
      DO i=nx-1,nx+1
        phi(i,j) = spline(8, phi_ref(i,j,:), 21600.0, t_sim)
      END DO
    END DO
    DO j=-1,1
      DO i=-1,nx+1
        phi(i,j) = spline(8, phi_ref(i,j,:), 21600.0, t_sim)
      END DO
    END DO
    DO j=ny-1,ny+1
      DO i=-1,nx+1
        phi(i,j) = spline(8, phi_ref(i,j,:), 21600.0, t_sim)
      END DO
    END DO
    
    !- Calculate boundary values of vorticity from geopotential
    DO j=0,ny
      zeta(0,j) = 1 / f(j) * ((phi(1,j) - 2 * phi(0,j) + phi(-1,j)) &
        / delta**2 + (phi(0,j+1) - 2 * phi(0,j) + phi(0,j-1)) / delta**2)
      zeta(nx,j) = 1 / f(j) * ((phi(nx+1,j) - 2 * phi(nx,j) + phi(nx-1,j)) &
        / delta**2 + (phi(nx,j+1) - 2 * phi(nx,j) + phi(nx,j-1)) / delta**2)
    END DO
    DO i=0,nx
      zeta(i,0) = 1 / f(0) * ((phi(i+1,0) - 2 * phi(i,0) + phi(i-1,0)) &
        / delta**2 + (phi(i,1) - 2 * phi(i,0) + phi(i,-1)) / delta**2)
      zeta(i,ny) = 1 / f(ny) * ((phi(i+1,ny) - 2 * phi(i,ny) + phi(i-1,ny)) &
        / delta**2 + (phi(i,ny+1) - 2 * phi(i,ny) + phi(i,ny-1)) / delta**2)
    END DO
    
    !- Calculate runtime
    CALL SYSTEM_CLOCK(count_bound, count_rate_bound)
    runtime_bound = runtime_bound + REAL(count_bound) / REAL(count_rate_bound) &
      - diff
  
  END SUBROUTINE get_boundary

  !--------------------------------Geopotential---------------------------------

  SUBROUTINE get_geopot()
  ! Calculate geopotential via SOR method

    INTEGER :: count_geo         !< count (needed for runtime)
    INTEGER :: count_rate_geo    !< count rate (needed for runtime)

    REAL :: diff = 0.0           !< difference (needed for runtime)
    REAL :: omega                !< relaxation factor of SOR method

    REAL(kind=8) :: phi_old(-1:nx+1,-1:ny+1)    !< geopotential of last
                                                !< iteration step

    !- Gauge runtime
    CALL SYSTEM_CLOCK(count_geo, count_rate_geo)
    diff = REAL(count_geo) / REAL(count_rate_geo)

    !- Calculate relaxation factor
    omega = 2 - 2 * pi / SQRT(2.0) * SQRT(1.0 / (nx + 1)**2 + 1.0 / (ny + 1)**2)
    
    !- Do SOR iteration
    DO k=1,k_max

      phi_old(:,:) = phi(:,:)

      !- Calculate new geopotential via SOR method
      DO j=1,ny-1
        DO i=0,nx
          
          !- Skip first and last step for sheet 3
          IF ((i == 0 .OR. i == nx) .AND. sheet == 3) THEN
            CYCLE
          END IF
          
          phi(i,j) = phi(i,j) * (1 - omega) + 0.25 * omega * (phi(i+1,j) &
            + phi(i-1,j) + phi(i,j+1) + phi(i,j-1) - f(j) * delta**2 &
            * zeta(i,j))
        
          !- Set cyclic boundary conditions for sheet 1 and 2
          IF (sheet == 1 .OR. sheet == 2) THEN 
            IF (i == 0) THEN
              phi(nx+1,j) = phi(0,j)
            END IF
            IF (i == nx) THEN
              phi(-1,j) = phi(nx,j)
            END IF
          END IF
          
        END DO  
      END DO
      
      !- Stop iteration if maximal difference between new and old geopotential
      !- is small enough
      IF (MAXVAL(ABS(phi(:,:) - phi_old(:,:))) < 0.001) THEN
        EXIT
      END IF
      
    END DO
    
    !- Calculate runtime
    CALL SYSTEM_CLOCK(count_geo, count_rate_geo)
    runtime_geo = runtime_geo + REAL(count_geo) / REAL(count_rate_geo) - diff

  END SUBROUTINE get_geopot
  
  !----------------------------------Vorticity----------------------------------

  SUBROUTINE get_vorticity()
  ! Calculate vorticity via prognostic equation

    INTEGER :: count_vor         !< count (needed for runtime)
    INTEGER :: count_rate_vor    !< count rate (needed for runtime)

    REAL :: diff = 0.0           !< difference (needed for runtime)

    !- Gauge runtime
    CALL SYSTEM_CLOCK(count_vor, count_rate_vor)
    diff = REAL(count_vor) / REAL(count_rate_vor)
    
    !- Calculate new vorticity via Euler method for first time step
    IF (t == 1) THEN
      DO j=1,ny-1
        DO i=0,nx
        
          !- Skip first and last step for sheet 3
          IF ((i == 0 .OR. i == nx) .AND. sheet == 3) THEN
            CYCLE
          END IF
        
          zeta_p(i,j) = zeta(i,j) - dt * (u(i,j) * (zeta(i+1,j) - zeta(i-1,j)) &
            / (2 * delta) + v(i,j) * (zeta(i,j+1) - zeta(i,j-1)) / (2 * delta) &
            + v(i,j) * beta)
            
        END DO
      END DO
    END IF
    
    !- Calculate new vorticity via leapfrog method for other time steps
    IF (t > 1) THEN
      DO j=1,ny-1
        DO i=0,nx
        
          !- Skip first and last step for sheet 3
          IF ((i == 0 .OR. i == nx) .AND. sheet == 3) THEN
            CYCLE
          END IF
        
          zeta_p(i,j) = zeta_m(i,j) - 2.0 * dt * (u(i,j) * (zeta(i+1,j) &
            - zeta(i-1,j)) / (2 * delta) + v(i,j) * (zeta(i,j+1) &
            - zeta(i,j-1)) / (2 * delta) + v(i,j) * beta)

        END DO
      END DO
    END IF

    !- Set Dirichlet boundary conditions at lower and upper boundary
    IF (sheet == 2) THEN
      zeta_p(:,0) = zeta(:,0)
      zeta_p(:,ny) = zeta(:,ny)
    END IF
    
    !- Set cyclic boundary conditions for sheet 2
    IF (sheet == 2) THEN
      zeta_p(-1,:) = zeta_p(nx,:)
      zeta_p(nx+1,:) = zeta_p(0,:)
    END IF

    !- Set vorticity at time t-1 and t
    zeta_m(:,:) = zeta(:,:)
    zeta(:,:) = zeta_p(:,:)
    
    !- Calculate runtime
    CALL SYSTEM_CLOCK(count_vor, count_rate_vor)
    runtime_vor = runtime_vor + REAL(count_vor) / REAL(count_rate_vor) - diff

  END SUBROUTINE get_vorticity

  !------------------------------------Wind-------------------------------------

  SUBROUTINE get_wind()
  ! Calculate wind speed components via geostrophic wind relation

    INTEGER :: count_wind         !< count (needed for runtime)
    INTEGER :: count_rate_wind    !< count rate (needed for runtime)

    REAL :: diff = 0.0            !< difference (needed for runtime)
    
    !- Gauge runtime
    CALL SYSTEM_CLOCK(count_wind, count_rate_wind)
    diff = REAL(count_wind) / REAL(count_rate_wind)

    !- Calculate wind components
    DO j=1,ny-1
      DO i=0,nx
      
        !- Skip first and last step for sheet 3
        IF ((i == 0 .OR. i == nx) .AND. sheet == 3) THEN
          CYCLE
        END IF

        u(i,j) = -1 / f(j) * (phi(i,j+1) - phi(i,j-1)) / (2 * delta)
        v(i,j) = 1 / f(j) * (phi(i+1,j) - phi(i-1,j)) / (2 * delta)
        
      END DO
    END DO
    
    !- Set wind at left and right boundary for sheet 3
    IF (sheet == 3) THEN
      u(0,:) = u(1,:)
      u(nx,:) = u(nx-1,:)
      v(0,:) = v(1,:)
      v(nx,:) = v(nx-1,:)
    END IF
    
    !- Set wind at lower and upper boundary
    u(:,0) = u(:,1)
    u(:,ny) = u(:,ny-1)
    v(:,0) = v(:,1)
    v(:,ny) = v(:,ny-1)

    !- Calculate runtime
    CALL SYSTEM_CLOCK(count_wind, count_rate_wind)
    runtime_wind = runtime_wind + REAL(count_wind) / REAL(count_rate_wind) &
      - diff

  END SUBROUTINE get_wind

  !------------------------------------Input------------------------------------

  SUBROUTINE read_input_data(input)
  ! Read input from mesoscale model
  
    CHARACTER(LEN=*), INTENT(IN) :: input    !< name of input file

    INTEGER, SAVE :: id_file      !< ID of NetCDF file
    INTEGER, SAVE :: id_var_in    !< ID of input array
    INTEGER, SAVE :: nc_stat      !< status flag of NetCDF routines

    REAL :: var_in(-1:nx+1,-1:ny+1,0:nt_ref)    !< input array

    !- Open NetCDF file
    nc_stat = NF90_OPEN(input, NF90_NOWRITE, id_file)

    !- Get ID of array to read
    nc_stat = NF90_INQ_VARID(id_file, "phi", id_var_in)

    !- Read array
    nc_stat = NF90_GET_VAR(id_file, id_var_in, var_in, start = (/1, 1, 1/), &
      count = (/nx+3, ny+3, nt_ref+1/))

    !- Resort input array and save to proper variable
    DO k=0,nt_ref
      DO j=-1,ny+1
        DO i=-1,nx+1
          phi_ref(i,j,k) = var_in(i,j,k)
        END DO
      END DO
    END DO

  END SUBROUTINE read_input_data

  !-----------------------------------Output------------------------------------

  SUBROUTINE data_output(action, output)
  ! Data output to NetCDF file
  
    CHARACTER(LEN=*), INTENT(IN) :: action    !< flag to steer routine
                                              !< (open/close/write)
    CHARACTER(LEN=*), INTENT(IN) :: output    !< output file name prefix
  
    INTEGER :: count_output         !< count (needed for runtime)
    INTEGER :: count_rate_output    !< count rate (needed for runtime)
    INTEGER :: t_ref = 0            !< time index of reference geopotential
  
    INTEGER, SAVE :: do_count = 0     !< counting output
    INTEGER, SAVE :: id_dim_time      !< ID of dimension time
    INTEGER, SAVE :: id_dim_x         !< ID of dimension x
    INTEGER, SAVE :: id_dim_y         !< ID of dimension time
    INTEGER, SAVE :: id_file          !< ID of NetCDF file
    INTEGER, SAVE :: id_var_phi       !< ID of geopotential
    INTEGER, SAVE :: id_var_phiref    !< ID of reference geopotential
    INTEGER, SAVE :: id_var_time      !< ID of time
    INTEGER, SAVE :: id_var_u         !< ID of wind speed along x
    INTEGER, SAVE :: id_var_v         !< ID of wind speed along y
    INTEGER, SAVE :: id_var_x         !< ID of x
    INTEGER, SAVE :: id_var_y         !< ID of y
    INTEGER, SAVE :: id_var_zeta      !< ID of vorticity
    INTEGER, SAVE :: nc_stat          !< status flag of NetCDF routines
  
    REAL :: diff = 0.0    !< difference (needed for runtime)
  
    REAL, DIMENSION(:), ALLOCATABLE :: netcdf_data_1d      !< 1D output array
  
    REAL, DIMENSION(:,:), ALLOCATABLE :: netcdf_data_2d    !< 2D output array
  
    !- Gauge runtime
    CALL SYSTEM_CLOCK(count_output, count_rate_output)
    diff = REAL(count_output) / REAL(count_rate_output)
  
    SELECT CASE (TRIM(action))
  
    !- Initialize output file
    CASE ('open')
  
      !- Delete any pre-existing output file
      OPEN(20, FILE=TRIM(output)//'.nc')
      CLOSE(20, STATUS='DELETE')
  
      !- Open file
      nc_stat = NF90_CREATE(TRIM(output)//'.nc', NF90_NOCLOBBER, id_file)
      IF (nc_stat /= NF90_NOERR) THEN
        WRITE(*, '(A)') '+++ netcdf error'
      END IF
  
      !- Write global attributes
      nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL, 'Conventions', 'COARDS')
      nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL, 'title', &
        'barotropic nwp-model')

      !- Define time coordinate
      nc_stat = NF90_DEF_DIM(id_file, 'time', NF90_UNLIMITED, id_dim_time)
      nc_stat = NF90_DEF_VAR(id_file, 'time', NF90_DOUBLE, id_dim_time, &
        id_var_time)
      nc_stat = NF90_PUT_ATT(id_file, id_var_time, 'units', &
        'seconds since 1900-1-1 00:00:00')
  
      !- Define spatial coordinates
      nc_stat = NF90_DEF_DIM(id_file, 'x', nx+1, id_dim_x)
      nc_stat = NF90_DEF_VAR(id_file, 'x', NF90_DOUBLE, id_dim_x, id_var_x)
      nc_stat = NF90_PUT_ATT(id_file, id_var_x, 'units', 'meters')
  
      nc_stat = NF90_DEF_DIM(id_file, 'y', ny+1, id_dim_y)
      nc_stat = NF90_DEF_VAR(id_file, 'y', NF90_DOUBLE, id_dim_y, id_var_y)
      nc_stat = NF90_PUT_ATT(id_file, id_var_y, 'units', 'meters')
  
      !- Define output arrays
      nc_stat = NF90_DEF_VAR(id_file, 'phi', NF90_DOUBLE, (/id_dim_x, &
        id_dim_y, id_dim_time/), id_var_phi)
      nc_stat = NF90_PUT_ATT(id_file, id_var_phi, 'long_name', &
        'geopotential at 500hPa')
      nc_stat = NF90_PUT_ATT(id_file, id_var_phi, 'short_name', 'geopotential')
      nc_stat = NF90_PUT_ATT(id_file, id_var_phi, 'units', 'm2/s2')
  
      nc_stat = NF90_DEF_VAR(id_file, 'phi_ref', NF90_DOUBLE, (/id_dim_x, &
        id_dim_y, id_dim_time/), id_var_phiref)
      nc_stat = NF90_PUT_ATT(id_file, id_var_phiref, 'long_name', &
        'reference geopotential at 500hPa')
      nc_stat = NF90_PUT_ATT(id_file, id_var_phiref, 'short_name', &
        'ref. geopotential')
      nc_stat = NF90_PUT_ATT(id_file, id_var_phiref, 'units', 'm2/s2')
  
      nc_stat = NF90_DEF_VAR(id_file, 'zeta', NF90_DOUBLE, (/id_dim_x, &
        id_dim_y, id_dim_time/), id_var_zeta)
      nc_stat = NF90_PUT_ATT(id_file, id_var_zeta, 'long_name', &
        'vorticity at 500hPa')
      nc_stat = NF90_PUT_ATT(id_file, id_var_zeta, 'short_name', &
        'vorticity')
      nc_stat = NF90_PUT_ATT(id_file, id_var_zeta, 'units', '1/s')
  
      nc_stat = NF90_DEF_VAR(id_file, 'u', NF90_DOUBLE, (/id_dim_x, id_dim_y, &
        id_dim_time/), id_var_u)
      nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'long_name', &
        'u component of wind')
      nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'short_name', 'u')
      nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'units', 'm/s')
  
      nc_stat = NF90_DEF_VAR(id_file, 'v', NF90_DOUBLE, (/id_dim_x, id_dim_y, &
        id_dim_time/), id_var_v)
      nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'long_name', &
        'v component of wind')
      nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'short_name', 'v')
      nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'units', 'm/s')
  
      !- Leave define mode
      nc_stat = NF90_ENDDEF(id_file)
  
      !- Write axis to file
      ALLOCATE(netcdf_data_1d(0:nx))
  
      !- x axis
      DO i=0,nx
        netcdf_data_1d(i) = i * delta
      ENDDO
      nc_stat = NF90_PUT_VAR(id_file, id_var_x, netcdf_data_1d, start = (/1/), &
        count = (/nx+1/))
  
      !- y axis
      DO j=0,ny
        netcdf_data_1d(j) = j * delta
      ENDDO
      nc_stat = NF90_PUT_VAR(id_file, id_var_y, netcdf_data_1d, start = (/1/), &
        count = (/ny+1/))
  
      DEALLOCATE(netcdf_data_1d)
  
    !- Close NetCDF file
    CASE ('close')
  
      nc_stat = NF90_CLOSE(id_file)
  
    !- Write data arrays to file
    CASE ('write')
    
      do_count = do_count + 1
      
      IF (t /= 0 .AND. sheet == 3) THEN
        IF (INT(t_sim / 21600) /= INT((t_sim - dt) / 21600)) THEN
          t_ref = t_ref + 1
        END IF
      END IF
  
      ALLOCATE(netcdf_data_2d(0:nx,0:ny))

      !- Write time
      nc_stat = NF90_PUT_VAR(id_file, id_var_time, (/t_sim/), &
        start = (/do_count/), count = (/1/))
      
      !- Write geopotential
      DO j=0,ny
        DO i=0,nx
          netcdf_data_2d(i,j) = phi(i,j)
        END DO
      END DO
      nc_stat = NF90_PUT_VAR(id_file, id_var_phi, netcdf_data_2d, &
        start = (/1, 1, do_count/), count = (/nx+1, ny+1, 1/))
      
      !- Write reference geopotential
      DO j=0,ny
        DO i=0,nx
          netcdf_data_2d(i,j) = phi_ref(i,j,t_ref)
        END DO
      END DO
      nc_stat = NF90_PUT_VAR(id_file, id_var_phiref, netcdf_data_2d, &
        start = (/1, 1, do_count/), count = (/nx+1, ny+1, 1/))
      
      !- Write vorticity
      DO j=0,ny
        DO i=0,nx
          netcdf_data_2d(i,j) = zeta(i,j)
        END DO
      END DO
      nc_stat = NF90_PUT_VAR(id_file, id_var_zeta, netcdf_data_2d, &
        start = (/1, 1, do_count/), count = (/nx+1, ny+1, 1/))
      
      !- Write u
      DO j=0,ny
        DO i=0,nx
          netcdf_data_2d(i,j) = u(i,j)
        END DO
      END DO
      nc_stat = NF90_PUT_VAR(id_file, id_var_u, netcdf_data_2d, &
        start = (/1, 1, do_count/), count = (/nx+1, ny+1, 1/))
  
      !- Write v
      DO j=0,ny
        DO i=0,nx
          netcdf_data_2d(i,j) = v(i,j)
        END DO
      END DO
      nc_stat = NF90_PUT_VAR(id_file, id_var_v, netcdf_data_2d, &
        start = (/1, 1, do_count/), count = (/nx+1, ny+1, 1/))
  
      DEALLOCATE(netcdf_data_2d)
  
    !- Print error message if unknown action selected
    CASE DEFAULT
  
      WRITE(*, '(A)') 'data_output: action'//TRIM(action)//'unknown!'
  
    END SELECT

    !- Calculate runtime
    CALL SYSTEM_CLOCK(count_output, count_rate_output)
    runtime_output = runtime_output + REAL(count_output) / &
    &REAL(count_rate_output) - diff

  END SUBROUTINE data_output

END PROGRAM nwp_program
